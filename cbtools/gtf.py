# -*- coding: utf-8 -*-

""" GTF utilities
"""

import re

attribute_regex = re.compile(r' *(?P<attr>[^";]+) "(?P<value>[^"]+)"')


class GTF_record():
    def __init__(self, seqname, source, feature, start, end, score, strand,
                 frame, attributes):
        self.seqname = seqname
        self.source = source
        self.feature = feature
        self.start = start
        self.end = end
        self.score = score
        self.strand = strand
        self.frame = frame
        self.attributes = attributes

    def __str__(self):
        attr_str = " ".join(['%s "%s";' %
                             (key, value) for
                             (key, value) in self.attributes.items()])
        return("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(
            self.seqname, self.source, self.feature, self.start, self.end,
            self.score, self.strand, self.frame, attr_str))

    @classmethod
    def from_gtf(cls, gtf_line):
        fields = gtf_line.strip().split('\t')
        attributes = dict(attribute_regex.findall(fields[8]))
        return(cls(*fields[0:8], attributes))
