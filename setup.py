#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup

setup(
    name='gtf_clean_genename',
    version='0.1.0',
    packages=['gtf_clean_genename'],
    entry_points={
        'console_scripts': [
            'gtf_clean_genename=gtf_clean_genename.__main__:main'
        ]
    })
