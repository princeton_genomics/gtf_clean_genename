#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" gtf_clean_genename

Ensure gene_name attribute is consistent for given gene_id in GTF file
"""

import argparse
import logging
from cbtools.gtf import GTF_record

__version__ = "0.1"
__author__ = "Lance Parsons"
__author_email__ = "lparsons@princeton.edu"
__copyright__ = "Copyright 2019, Lance Parsons"
__license__ = "MIT https://opensource.org/licenses/MIT"


def main():

    parser = argparse.ArgumentParser(
        description="Ensure gene_name attribute is consistent for given "
        "gene_id in GTF file",
        epilog="As an alternative to the commandline, params can be placed in "
        "a file, one per line, and specified on the commandline like "
        "'%(prog)s @params.conf'.",
        fromfile_prefix_chars='@')
    parser.add_argument("gtf_filename",
                        help="GTF Filename",
                        metavar="FILENAME")
    parser.add_argument("-v", "--verbose",
                        help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--version", action="version",
                        version="%(prog)s " + __version__)
    args = parser.parse_args()

    # Setup logging
    if args.verbose:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO

    logging.basicConfig(format="%(levelname)s: %(message)s", level=loglevel)

    logging.debug("Reading from {}".format(args.gtf_filename))
    current_gene_id = None
    current_gene_name = None
    current_records = []
    name_mismatch = False
    with open(args.gtf_filename, 'rU') as gtf_file:
        for line in gtf_file:
            record = GTF_record.from_gtf(line)
            if record.attributes['gene_id'] != current_gene_id:
                # New gene id
                print_records(current_records, name_mismatch)
                current_gene_id = record.attributes['gene_id']
                current_gene_name = record.attributes.get('gene_name', None)
                current_records = []
                name_mismatch = False
            else:
                # Same gene id
                if (record.attributes.get('gene_name', None)
                        != current_gene_name):
                    name_mismatch = True
            current_records.append(record)
        print_records(current_records, name_mismatch)


def remove_gene_names(gtf_records):
    for record in gtf_records:
        record.attributes.pop('gene_name', None)
    return gtf_records


def print_records(gtf_records, remove_name):
    records = gtf_records
    if remove_name:
        records = remove_gene_names(gtf_records)
    for record in records:
        print(record)


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
